import { Component } from '@angular/core';
import { Car } from './car';
import { TransportationService } from "./transportation.service";
import "reflect-metadata";
import { Observable } from 'rxjs';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  counter = 0;
  phrase = "It's going...";
  username: string;
  
  cars: Car[];
  carMake: string;
  carModel: string;
  carMiles: number;
  
  constructor(private transportationService: TransportationService) {
    transportationService.getCars()
    .subscribe(
      (results) => {this.cars=results},
      (error) => {console.log(error)},
      () => {console.log('done fetch');}
    )
  }
  
  increment() {
    this.counter++;
  }

  updatePhrase() {
    this.phrase += " and going... "+this.counter;
  }

  addCar() {
    const car: Car = { 
      make: this.carMake,
      model: this.carModel,
      miles: this.carMiles };
    this.transportationService.addCar(car).subscribe(
      (result) => console.log("Result of pushing car",result),
      (error) => console.log("error pushing car",error)
    );
  }

}
