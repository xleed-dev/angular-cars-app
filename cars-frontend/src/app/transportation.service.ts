import { Injectable } from '@angular/core';
import { Car } from "./car";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class TransportationService {

  private apiURL = 'http://localhost:3000/cars';

  constructor(private http: HttpClient) { }

  getCars(): Observable<Car[]> {
    console.log('fetching cars');
    return this.http.get<Car[]>(this.apiURL);
  }

  addCar(car: Car): Observable<Car> {
    console.log("sending car to back end");
    
    return this.http.post<Car>(this.apiURL,car);
  }
}
