import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Cars } from './car.entity';

@Injectable()
export class CarServices {
    constructor(@InjectRepository(Cars) private readonly carsRepository: Repository<Cars>) {
        this.carsRepository = carsRepository;
    }

    async findAll(): Promise<Cars[]> {
        return this.carsRepository.find();
    }

    async findOne(id: number): Promise<Cars> {
        return this.carsRepository.findOne(id);
    }

    async create(car: Cars): Promise<Cars> {
        return this.carsRepository.save(car);
    }
}