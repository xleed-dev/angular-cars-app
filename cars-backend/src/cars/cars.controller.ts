import { Body, Controller, Get, Post, Req, Request, Param } from '@nestjs/common';
import { CarServices } from "./car.service";

@Controller('cars')
export class CarsController {

    constructor(private readonly CarService: CarServices) { }

    @Get()
    findAll(@Req() request: Request): {} {
        return this.CarService.findAll();
    }
    
    @Get('/:id')
    findOne(@Req() request: Request, @Param() params): {} {
        return this.CarService.findOne(params.id);;
    }
    
    @Get("models")
    models(@Req() request: Request): string {
        return 'list of car models...'
    }

    @Post()
    async create(@Body() carParams) {
        return this.CarService.create(carParams)
    }

    @Post(':id')
    async update(@Body() carParams, @Param() params) {
        return `edit of ${carParams.make} id = ${params.id}`
    }
    
    @Post(':id/delete')
    async delete(@Param() params) {
        return `deleting car with id ${params.id}`
    }
}
