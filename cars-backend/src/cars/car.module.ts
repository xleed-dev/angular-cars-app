import { Module } from '@nestjs/common';
import { CarServices } from './car.service';
import { CarsController } from './cars.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Cars } from './car.entity';

@Module({
    imports: [TypeOrmModule.forFeature([Cars])],
    providers: [CarServices],
    controllers: [CarsController],
})
export class CarModule { }