import { Cars } from './cars/car.entity';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CarModule } from './cars/car.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'localhost',
      port: 5432,
      username: 'postgres',
      password: 'postgres',
      database: 'transportation',
      entities: [Cars],
      synchronize: true
    }),
    CarModule
  ]
})
export class AppModule {}
